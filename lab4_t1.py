import sys
from collections import defaultdict
from prettytable import PrettyTable

def calculate_freq(path: str):
    global a
    freq_dict = defaultdict(int)
    for k in a:
        freq_dict[k]
    with open(path, 'rt', encoding="utf8") as file:
        for line in file:
            for letter in line.lower():
                if letter in freq_dict.keys():
                    freq_dict[letter]+=1
    return freq_dict, {k: v for k, v in sorted(freq_dict.items(), key=lambda item: item[1], reverse=True)}

a='абвгґдеєжзиіїйклмнопрстуфхцчшщьюя '
while True:
    arg=input('Name(path) of file: ')
    dict_alphabeth, dict_freq=calculate_freq(arg)
    quan = sum(dict_freq.values())
    frt = PrettyTable(['Letter', 'rel freq of occur (by freq)', 'Letter_','Rel freq of occur (by alphab)'])
    i, ka, kf = 0, list(dict_alphabeth.keys()), list(dict_freq.keys())
    for k,v in dict_freq.items():
        frt.add_row([k,f'{v/quan:1.22f}',ka[i],f'{dict_alphabeth[ka[i]]/quan:1.22f}'])
        i+=1
    print('File: {0}'.format(arg))
    print('A chain of letters in order of decreasing frequency: ' + ''.join(kf))
    print(frt)

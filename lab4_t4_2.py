def key_by_val(letter):
    global alphabet
    for k,v in enumerate(alphabet):
        if v == letter:
            return k
        
alphabet='АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЬьЮюЯя'

def encrypt(text: str, a: int, b: int, m: int):
    res=''
    for l in text:
        if l not in alphabet:
            res += l
        else:
            res += alphabet[(a*key_by_val(l) + b) % m]
    return res

def gcdExtended(a, b):
    if a == 0 :
        return b,0,1
    gcd,x1,y1 = gcdExtended(b%a, a)
    x = y1 - (b//a) * x1
    y = x1
    return gcd,x,y

def decrypt(enc_text: str,  a: int, b: int, m: int):
    gcd, x, y= gcdExtended(a, m)
    res = ''
    amo = ((x % m + m) % m) if gcd == 1 else -1
    for l in enc_text:
        if l not in alphabet:
            res += l
        else:
            val = (amo*(key_by_val(l) + m - b))%m
            res += alphabet[val]
            
    return res

def main():
    while True:
        file = input("Enter path to file: ")
        a, b, m = int(input('a: ')), int(input('b: ')), int(input('m: '))
        choice = input('(d)ecrypt/(e)ncrypt?: ')
        with open(file, 'r', encoding='utf8') as f:
            text = f.read()
            if choice.startswith('d'):
                print(decrypt(text, a, b, m))
            elif choice.startswith('e'):
                print(encrypt(text, a, b, m))
            else:
                print('pnx')

if __name__=="__main__":
    main()

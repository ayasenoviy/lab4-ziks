from openpyxl import Workbook
from openpyxl.chart import (
    PieChart,
    Reference
)
from openpyxl.chart.series import DataPoint
import sys
from collections import defaultdict
from prettytable import PrettyTable

def calculate_freq(path: str):
    global tri
    global a
    freq_dict = defaultdict(int)
    for k in tri:
        freq_dict[k]
    with open(path, 'rt', encoding='utf8') as file:
        lines_pre = file.read() 
    lines = [el for el in lines_pre if el in a]
    trigram_lines = [''.join(lines[i:i+3]) for i in range(0, len(lines), 3)]
    for trigram in trigram_lines:
        freq_dict[trigram]+=1
    total = sum(freq_dict.values())
    for k in freq_dict.keys():
        freq_dict[k]/=total 
    return {k: v for k, v in sorted(freq_dict.items(), key=lambda item: item[1], reverse=True)}

def draw_diagram(data: list):
    wb = Workbook()
    ws = wb.active
    ws.append(['Threegramm','rel freq x1000'])
    for row in data:
        ws.append([row[0], float(row[1])*1000])
    pie = PieChart()
    labels = Reference(ws, min_col=1, min_row=2, max_row=31)
    data = Reference(ws, min_col=2, min_row=1, max_row=31)
    pie.add_data(data, titles_from_data=True)
    pie.set_categories(labels)
    pie.title = "top-30 threegram"
    slice = DataPoint(idx=0, explosion=20)
    pie.series[0].data_points = [slice]
    ws.add_chart(pie, "D1")
    return wb

def draw_table(colsdt=34):
    global dict_freq
    wb = Workbook()
    ws = wb.create_sheet(title = 'table_freq')
    fdl = sum([[k,f"{v:.6f}"] for k,v in dict_freq.items() if v != 0],[])
    head = sum([['т'+str(i),'ч'+str(i)] for i in range(int(colsdt/2))],[])
    frt = PrettyTable(head)
    ws.append(head)
    for i in range(0, len(fdl), colsdt):
        frt.add_row(fdl[i:i+colsdt])
        ws.append(fdl[i:i+colsdt])
    return frt, fdl, wb

a,tri=['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'],[]
for i in a:
    for j in a:
        for k in a:
            if i+j+k not in tri:
                tri.append(i+j+k) 
while True:
    arg=input('Name(path) of file: ')
    filename = arg.split("/")[-1]
    calculate_freq(arg)
    dict_freq=calculate_freq(arg)
    frt,fdl,wbt=draw_table(2)
    wbt.save(f'{filename}_3_e.xlsx')
    print('3e: saved as ' + filename + '_3_e.xlsx')
    print('3f: ' + str(fdl[0:60:2]))
    wbd = draw_diagram([fdl[i:i+2] for i in range(0, len(fdl[0:60]), 2)])
    wbd.save(f'{filename}_3_g.xlsx')
    print('3g: saved as ' + filename + '_3_g.xlsx')

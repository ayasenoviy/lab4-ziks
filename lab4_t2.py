from openpyxl import Workbook
from openpyxl.chart import (
    PieChart,
    Reference
)
from openpyxl.chart.series import DataPoint
import sys
from collections import defaultdict
from prettytable import PrettyTable

def calculate_freq(path: str):
    global bi
    global a
    freq_dict = defaultdict(int)
    for k in bi:
        freq_dict[k]
    with open(path, 'rt', encoding="utf8") as file:
        lines_pre = file.read().lower()
    lines = [el for el in lines_pre if el in a]
    bigrams_lines = [''.join(lines[i:i+2]) for i in range(0, len(lines), 2)]
    for bigram in bigrams_lines:
        freq_dict[bigram]+=1
    total = sum(freq_dict.values())
    for k in freq_dict.keys():
        freq_dict[k]/=total 
    return {k: v for k, v in sorted(freq_dict.items(), key=lambda item: item[1], reverse=True)}

def draw_diagram(data: list):
    wb = Workbook()
    ws = wb.active
    ws.append(['Bigramm','rel freq x1000'])
    for row in data:
        ws.append([row[0], float(row[1])*1000])
    pie = PieChart()
    labels = Reference(ws, min_col=1, min_row=2, max_row=31)
    data = Reference(ws, min_col=2, min_row=1, max_row=31)
    pie.add_data(data, titles_from_data=True)
    pie.set_categories(labels)
    pie.title = "top-30 bigramm"
    slice = DataPoint(idx=0, explosion=20)
    pie.series[0].data_points = [slice]
    ws.add_chart(pie, "D1")
    return wb

def draw_table(colsdt=34):
    global dict_freq
    wb = Workbook()
    ws = wb.create_sheet(title = 'table_freq')
    fdl = sum([[k,f"{v:.6f}"] for k,v in dict_freq.items() if v != 0],[])
    head = sum([['б'+str(i),'ч'+str(i)] for i in range(int(colsdt/2))],[])
    frt = PrettyTable(head)
    ws.append(head)
    for i in range(0, len(fdl), colsdt):
        frt.add_row(fdl[i:i+colsdt])
        ws.append(fdl[i:i+colsdt])
    return frt, fdl, wb

a,bi='абвгґдеєжзиіїйклмнопрстуфхцчшщьюя',[]
#'АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЬьЮюЯя'
for i in a:
    for j in a:
        if i+j not in bi: 
            bi.append(i+j) 
files=['avtobus.txt']
while True:
    arg=input('Name(path) of file: ')
    filename = arg.split("/")[-1]
    calculate_freq(arg)
    dict_freq=calculate_freq(arg)
    frt,fdl,wbt=draw_table(4)
    wbt.save(f'{filename}_2_a.xlsx')
    print('2a: saved as ' + filename + '_2_a.xlsx')
    print('2b: ' + ', '.join(fdl[0:60:2]))
    wbd = draw_diagram([fdl[i:i+2] for i in range(0, len(fdl[0:60]), 2)])
    wbd.save(f'{filename}_2_c.xlsx')
    print('2c: saved as ' + filename + '_2_c.xlsx')
    wb = Workbook()
    ws = wb.create_sheet(title = 'matrix')
    ws = wb['matrix']
    ws.append([''] + [e for e in a])
    for symb_row in a:
        curr=[symb_row]
        for symb_col in a:
            curr.append(dict_freq[symb_row+symb_col])
        ws.append(curr)
    wb.save(f'{filename}_2_d.xlsx')
    print('2d: saved as ' + filename + '_2_d.xlsx')
    
def key_by_val(letter):
    global alphabet
    for k,v in enumerate(alphabet):
        if v == letter:
            return k
        
def capital_letter_control(dcrpd):
    capitals = 0
    for l in dcrpd:
        if l.isupper():
            capitals+=1
    return True if capitals*100 / len(dcrpd) < 8 else False

def is_prime(num):

    if num == 0 or num == 1:
        return False
    for x in range(2, num):
        if num % x == 0:
            return False
    else:
        return True
    
alphabet='АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЬьЮюЯя'

def gcdExtended(a, b):
    if a == 0 :
        return b,0,1
    gcd,x1,y1 = gcdExtended(b%a, a)
    x = y1 - (b//a) * x1
    y = x1
    return gcd,x,y

def decrypt(enc_text: str,  a: int, b: int, m: int):
    gcd, x, y= gcdExtended(a, m)
    res = ''
    amo = ((x % m + m) % m) if gcd == 1 else -1
    for l in enc_text:
        if l not in alphabet:
            res += l
        else:
            val = (amo*(key_by_val(l) + m - b))%m
            res += alphabet[val]
            
    return res

def main():
    while True:
        file = input("Enter path to file: ")
        m = 66
        cin, cout = key_by_val(input("The letter was: ")), key_by_val(input("The letter became: "))
        primes = [c for c in range(0,m) if is_prime(c)]
        with open(file, 'r', encoding='utf8') as f:
            text = f.read()
            for j in primes:
                for h in range(0,100):
                    if (cin*j+h)%66 == cout:
                        dcrpd = decrypt(text, j, h, m)
                        if capital_letter_control(dcrpd):
                            print('#'*50 + f'\nParameters: a = {j}, b = {h}. A variant of how the text can look:\n' + '#'*50)
                            print(decrypt(text, j, h, m)[:70] + '.....')
                        break

if __name__=="__main__":
    main()